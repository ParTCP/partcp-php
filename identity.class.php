<?php

/*
	This file is part of the ParTCP PHP Library project
	Copyright (C) 2023 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Identity {

	static $storage = 'ParTCP_Key_Storage_Fs';
	public $id;
	public $pubKey;

}


class ParTCP_Public_Identity extends ParTCP_Identity {

	public function __construct( $id, $retrievePubKey = FALSE, $forceRetrieve = FALSE ){
		$this->id = $id;
		$this->pubKey = self::$storage::get_pubkey( $id );
		if ( $retrievePubKey && ( $forceRetrieve || ! $this->pubKey ) ){
			if ( strpos( $id, '@' ) ){
				$this->retrieve_user_pubkey();
			}
			else {
				$this->retrieve_server_pubkey();
			}
			if ( $this->pubKey ){
				self::$storage::store_pubkey( $this->id, $this->pubKey );
			}
		}
	}

	private function retrieve_user_pubkey(){
		list ( $name, $server ) = explode( '@', $this->id );
		if ( strpos( $name, '+' ) ){
			list ( $eventId, $name ) = explode( '+', $name );
		}
		$message = new ParTCP_Outgoing_Message( $server, NULL, [
			'Message-Type' => 'participant-details-request',
			'Participant-Id' => $name,
		]);
		if ( ! empty( $eventId ) ){
			$message->set( 'Event-Id', $eventId );
		}
		$result = $message->send();
		if ( ! $result || $result['status'] != 200 ){
			return;
		}
		$response = new ParTCP_Incoming_Message( $result['body'] );
		$data = $response->get('Participant-Data');
		$this->pubKey = $data['public_key'] ?? NULL;
	}

	private function retrieve_server_pubkey(){
		$message = new ParTCP_Outgoing_Message( $this->id, NULL,
			'server-details-request' );
		$result = $message->send();
		if ( ! $result || $result['status'] != 200 ){
			return;
		}
		$response = new ParTCP_Incoming_Message( $result['body'] );
		$data = $response->get('Server-Data');
		$this->pubKey = $data['public_key'] ?? $response->get('Public-Key');
	}
}


class ParTCP_Private_Identity extends ParTCP_Identity {

	public $privKey;

	public function __construct( $id, $create = FALSE, $persistent = FALSE ){
		$this->id = $id;
		$keyPair = self::$storage::get_keypair( $id );
		if ( ! $keyPair && $create ){
			$keyPair = ParTCP_Crypto::generate_keys();
			if ( $persistent ){
				self::$storage::store_keypair( $id, $keyPair[0], $keyPair[1] );
			}
		}
		if ( $keyPair ){
			$this->pubKey = $keyPair[0];
			$this->privKey = $keyPair[1];
		}
	}
}


// end of file identity.class.php

