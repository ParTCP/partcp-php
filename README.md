# partcp-php

Library for leveraging the ParTCP protocol in PHP projects


## Requirements

- PHP 8+ with OpenSSL support, yaml and sodium extension


## Usage

The `ParTCP` class provides high-level functions and is the preferred
way to make use of the ParTCP library:

```php
require_once 'partcp.class.php';
$pt = new ParTCP( '/path/to/writable/dir' );
$pt->set_remote_id('demo.partcp.org');
if ( $pt->ping() ){
	echo 'Successfully connected to demo server';
}
```

If you want more control, you can use the low-level objects of
the ParTCP library. First, load the individual class files:

```php
require_once 'crypto.class.php';
require_once 'key_storage.class.php';
require_once 'identity.class.php';
require_once 'incoming_message.class.php';
require_once 'outgoing_message.class.php';
```

Set the base directory for storing identities:

```php
ParTCP_Key_Storage::$storageDir = '/path/to/writable/directory';
```

Or, if you want to use another class for storing keys, register it
with the ParTCP_Identity class:

```php
ParTCP_Identity::$storage = 'ParTCP_Key_Storage_Mem';
```

Create a private (local) identity and submit the public key to
a remote server:

```php
$localId = new ParTCP_Private_Identity( 'name@server.tld', TRUE, TRUE );
$remoteId = new ParTCP_Public_Identity( 'server.tld', TRUE );
$msg = new ParTCP_Outgoing_Message( $remoteId, $localId, [
   'Message-Type' => 'key-submission',
   'Credential~~' => $credential,
   'Public-Key' => $localId->pubKey,
]);
$msg->set_date();
$response = $msg->send();
```

Check the response:

```php
if ( $response['status'] != 200 ){
   die('Could not connect to server');
}

$msg = new ParTCP_Incoming_Message( $response['body'] );
if ( $msg->get('Message-Type') == 'Rejection-Notice' ){
   die( 'Key submission rejected: ' . $msg->get('Rejection-Reason') );
}

$result = $msg->get_signature_status();
if ( ! $result ){
   die( 'Signature error: ' . $msg->signatureStatusMessage );
}
```

Use low-level crypto functions:

```php
ParTCP_Crypto::set_remote_pubkey( $pubKey );
ParTCP_Crypto::set_local_privkey( $privKey );

$signature = ParTCP_Crypto::generate_signature( $data );
$verified = ParTCP_Crypto::verify_signature( $signature );

$encrypted = ParTCP_Crypto::encrypt( $data );
$decrypted = ParTCP_Crypto::decrypt( $encrypted );
```

