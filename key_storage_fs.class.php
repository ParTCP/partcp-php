<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2022 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Key_Storage_Fs {

	public static $storageDir;


	public static function store_pubkey( $id, $pubKey ){
		$file = self::id2path( $id );
		$data = $pubKey;
		self::store_file( $file, $data );
	}


	public static function get_pubkey( $id ){
		$file = self::id2path( $id );
		if ( ! file_exists( $file ) ){
			return FALSE;
		}
		return trim( file_get_contents( $file ) );
	}


	public static function list_pubkeys( $server = NULL ){
		$dir = self::$storageDir . '/public' . ( $server ? "/{$server}" : '' );
		if ( ! is_dir( $dir ) ){
			return [];
		}
		$list = array_filter( scandir( $dir ), function( $file ) use ( $dir ){
			return $file[0] != '.' && is_file( "{$dir}/{$file}" );
		});
		return array_map( function( $f ){ return basename( $f, '.key' ); }, $list );
	}


	public static function delete_pubkey( $id ){
		$file = self::id2path( $id );
		if ( ! file_exists( $file ) ){
			return FALSE;
		}
		return unlink( $file );
	}


	public static function store_keypair( $id, $pubKey, $privKey ){
		$file = self::id2path( $id, TRUE );
		if ( ! $file ){
			return FALSE;
		}
		$data = [ $pubKey, $privKey ];
		self::store_file( $file, json_encode( $data ) );
	}


	public static function exists_keypair( $id ){
		$file = self::id2path( $id, TRUE );
		return $file && file_exists( $file );
	}


	public static function get_keypair( $id ){
		$file = self::id2path( $id, TRUE );
		if ( ! $file || ! file_exists( $file ) ){
			return FALSE;
		}
		return json_decode( file_get_contents( $file ) );
	}


	public static function list_keypairs( $server = NULL ){
		$dir = self::$storageDir . '/private';
		if ( $server ){
			$dir .= '/' . trim( $server, '/' );
			if ( ! is_dir( $dir ) ){
				return [];
			}
			chdir( $dir );
			$list = glob( '*.keys' );
			return array_map( function( $file ) use ( $server ){
				$file = basename( $file, '.keys' );
				return "{$file}@{$server}";
			}, $list );
		}
		else {
			chdir( $dir );
			$list = glob( '**/*.keys' );
			return array_map( function( $file ) use ( $server ){
				$parts = explode( '/', $file );
				$name = basename( $parts[1], '.keys' );
				return "{$name}@{$parts[0]}";
			}, $list );
		}
	}


	public static function delete_keypair( $id ){
		$file = self::id2path( $id, TRUE );
		if ( ! file_exists( $file ) ){
			return FALSE;
		}
		return unlink( $file );
	}


	private static function id2path( $id, $private = FALSE ){
		$parts = explode( '@', $id );
		$path = $private ? '/private/' : '/public/';
		if ( ! empty( $parts[1] ) ){
			$path .= "{$parts[1]}/";
		}
		$path .= str_replace( '/', '|', $parts[0] );
		$path .= $private ? '.keys' : '.key';
		return self::$storageDir . $path;
	}


	private static function store_file( $file, $data ){
		$dir = dirname( $file );
		if ( ! file_exists( $dir ) && ! mkdir( $dir, 0700, TRUE ) ){
			throw new Exception( "Could not create directory {$dir}" );
		}
		if ( ! file_put_contents( $file, $data ) ){
			throw new Exception( "Could not write file {$file}" );
		}
		chmod( $file, 0600 );
	}

}


// end of file key_storage_fs.class.php

