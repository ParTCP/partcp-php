<?php

/*
	This file is part of the ParTCP Server project
	Copyright (C) 2024 Martin Wandelt

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

class ParTCP_Key_Storage_Mem {

	static $pubKeys = [];
	static $keyPairs = [];

	public static function store_pubkey( $id, $pubKey ){
		self::$pubKeys[ $id ] = $pubKey;
	}


	public static function get_pubkey( $id ){
		return self::$pubKeys[ $id ] ?? NULL;
	}


	public static function delete_pubkey( $id ){
		unset( self::$pubKeys[ $id ] );
		return TRUE;
	}


	public static function store_keypair( $id, $pubKey, $privKey ){
		self::$keyPairs[ $id ] = [ $pubKey, $privKey ];
	}


	public static function exists_keypair( $id ){
		return ! empty( self::$keyPairs[ $id ] );
	}


	public static function get_keypair( $id ){
		return self::$keyPairs[ $id ] ?? NULL;
	}


	public static function list_keypairs( $server = NULL ){
		$keys = self::$keyPairs;
		if ( $server ){
			echo $server . "\n";
			$keys = array_filter( $keys,
				function( $k ) use ( $server ){
					return strrchr( $k, '@' ) == "@{$server}";
				},
				ARRAY_FILTER_USE_KEY );
		};
		return array_keys( $keys );
	}


	public static function delete_keypair( $id ){
		unset( self::$keyPairs[ $id ] );
		return TRUE;
	}

}


// end of file key_storage_mem.class.php

